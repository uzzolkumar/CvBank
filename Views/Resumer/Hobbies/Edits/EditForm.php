
<?php
use  App\Resumer\Hobbies\Hobbies;
include '../../../../vendor/autoload.php';

$obj=new Hobbies();

if(isset($_SESSION['fail']))
{

    $error =$_SESSION['fail'] ;
}

if(isset($_SESSION['user_info']))
{
    $userid =$_SESSION['user_info']['id'];

}
else{
    $_SESSION['fail']='Sorry ! You are Not Authorized in this page ';

    include '../../Logform/index.php';
}

if(isset($_GET['uniqueid']))
{
    $uniqueid =$_GET['uniqueid'];

    if($uniqueid && $userid )
    {
        $data =$obj->setData($_POST,$userid)->index();
    }
}


?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CVbank</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../dashboard_style/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../dashboard_style/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../../dashboard_style/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../dashboard_style/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.html">SB Admin</a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
                <ul class="dropdown-menu message-dropdown">
                    <li class="message-preview">
                        <a href="#">
                            <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                <div class="media-body">
                                    <h5 class="media-heading"><strong>John Smith</strong>
                                    </h5>
                                    <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="message-preview">
                        <a href="#">
                            <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                <div class="media-body">
                                    <h5 class="media-heading"><strong>John Smith</strong>
                                    </h5>
                                    <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="message-preview">
                        <a href="#">
                            <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                <div class="media-body">
                                    <h5 class="media-heading"><strong>John Smith</strong>
                                    </h5>
                                    <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="message-footer">
                        <a href="#">Read All New Messages</a>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                <ul class="dropdown-menu alert-dropdown">
                    <li>
                        <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                    </li>
                    <li>
                        <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
                    </li>
                    <li>
                        <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
                    </li>
                    <li>
                        <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
                    </li>
                    <li>
                        <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
                    </li>
                    <li>
                        <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">View All</a>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> John Smith <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li>
                    <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-cog"></i> Settings <i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="demo" class="collapse">
                        <li>
                            <a href="../../Settings/View/index.php">View</a>
                        </li>
                        <li>
                            <a href="../../Settings/insert/Create.php">Insert</a>
                        </li>
                        <li>
                            <a href="../../Settings/Edits/index.php">Edit</a>
                        </li>
                        <li>
                            <a href="../../Settings/Delete/index.php">Delete</a>
                        </li>

                    </ul>
                </li>
                <li>
                    <a href="javascript:;" data-toggle="collapse" data-target="#dem"><i class=" fa fa-credit-card"></i> Abouts <i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="dem" class="collapse">
                        <li>
                            <a href="../../Abouts/View/index.php">View</a>
                        </li>
                        <li>
                            <a href="../../Abouts/insert/Create.php">Insert</a>
                        </li>
                        <li>
                            <a href="../../Abouts/Edits/index.php">Edit</a>
                        </li>
                        <li>
                            <a href="../../Abouts/Delete/delete.php">Delete</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="javascript:;" data-toggle="collapse" data-target="#de"><i class="fa fa-cube"></i> Hobbies <i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="de" class="collapse">
                        <li>
                            <a href="../../Hobbies/View/index.php">View</a>
                        </li>
                        <li>
                            <a href="../../Hobbies/View/index.php">Insert</a>
                        </li>
                        <li>
                            <a href="../../Hobbies/Edits/index.php">Edit</a>
                        </li>
                        <li>
                            <a href="../../Hobbies/Delete/index.php">Delete</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;" data-toggle="collapse" data-target="#d"><i class="fa fa-graduation-cap"></i> Education <i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="d" class="collapse">
                        <li>
                            <a href="../../Educations/View/index.php">View</a>
                        </li>
                        <li>
                            <a href="../../Educations/insert/Create.php">Insert</a>
                        </li>
                        <li>
                            <a href="../../Educations/Edits/index.php">Edit</a>
                        </li>
                        <li>
                            <a href="../../Educations/Delete/index.php">Delete</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Dropdown <i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="demo" class="collapse">
                        <li>
                            <a href="#">Dropdown Item</a>
                        </li>
                        <li>
                            <a href="#">Dropdown Item</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="blank-page.html"><i class="fa fa-fw fa-file"></i> Blank Page</a>
                </li>
                <li>
                    <a href="index-rtl.html"><i class="fa fa-fw fa-dashboard"></i> RTL Dashboard</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">

        <div class="container-fluid" style="height: 600px;">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-14">
                    <div class="navbar navbar-inverse bg-info-700 navbar-component" style="position: relative; z-index: 100;">
                        <div class="navbar-header">
                            <ul class="nav navbar-nav pull-right visible-xs-block">
                                <li><a data-toggle="collapse" data-target="#navbar-demo-mobile"><i class="icon-grid3"></i></a></li>
                            </ul>
                        </div>

                        <div class="navbar-collapse collapse" id="navbar-demo-mobile">

                            <ul class="nav navbar-nav navbar-right">
                                <li class=""><a href="../View/index.php">View</a></li>
                                <li><a href="update.php">Insert</a></li>
                                <li><a href="../Edits/index.php">Edit</a></li>
                                <li><a href="../Delete/delete.php">Delete</a></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li><a href="#">One more line</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div id="page-wrapper">

                        <div class="container-fluid">



                            <div class="row">
                                <div class="col-lg-12">

                                    <form method="post" action="update.php">

                                        <div class="form-group">
                                            <label>Title : </label>
                                            <input  type="text" class="form-control" name="title" value="<?php if(!empty($data)) {echo $data['title'];}?>">
                                            <?php  if(isset($error['title1'])) {echo $error['title1']; }
                                            ?>
                                        </div>
                                        <div class="form-group">
                                            <label>Description :</label>
                                            <input  type="text" class="form-control" name="description" value="<?php if(!empty($data)) { echo $data['description'];}?>">
                                            <?php if(isset($error['des']))
                                            { echo $error['des']; }

                                            ?>
                                        </div>

                                        <div class="form-group">
                                            <label> Upload Image : </label>
                                          <input type="file" name="img" />
                                            <?php

                                            if(isset($error['img1'])) { echo $error['img1']; }


                                            unset($_SESSION['fail']);
                                            ?>
                                        </div>

                                        <input  type="submit"  value='Update' class="btn btn-default" />
                                        <button type="reset" class="btn btn-default">Reset </button>

                                </div>
                                </form>


                                <!-- /.row -->

                            </div>

                            <h3 class="alert-success">
                                <?php if(isset($_SESSION['message']))
                                {
                                    echo $_SESSION['message'];

                                    unset($_SESSION['message']);
                                }
                                ?>


                            </h3>
                            <!-- /.container-fluid -->

                        </div>
                        <!-- /#page-wrapper -->

                    </div>
                    <!-- /#wrapper -->
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../../dashboard_style/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../../dashboard_style/js/bootstrap.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="../../dashboard_style/js/plugins/morris/raphael.min.js"></script>
<script src="../../dashboard_style/js/plugins/morris/morris.min.js"></script>
<script src="../../dashboard_style/js/plugins/morris/morris-data.js"></script>

</body>

</html>
