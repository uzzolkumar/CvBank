<?php
namespace App\Resumer\Abouts;

use App\Resumer\Abouts\Abouts;

class validation
{
    public $fname = '';
    public $sname = '';
    public $id = '';
    public $email = '';
    public $phone = '';
    public $program ='';
    public  $bio ='';
    public $title='';
    public $error = '';


    public function setData($data = '')
    {
        if (array_key_exists('fname', $data)) {
            $this->fname = $data['fname'];
        }
        if (array_key_exists('sname', $data)) {
            $this->sname = $data['sname'];
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('phone', $data)) {
            $this->phone = $data['phone'];
        }
        if (array_key_exists('title', $data)) {
            $this->title = $data['title'];
        }
        if (array_key_exists('bio', $data)) {
         $this->bio = $data['bio'];
        }

        if (array_key_exists('droopDown', $data)) {
            $this->program = $data['droopDown'];
        }

        return $this;
    }

    public function validForInsert()
    {
        $this->error = array();
        if (isset($_POST)) {
            if (empty($this->phone)) {
                $this->error['phone1'] = "<span style='color:red'>" . "Phone  can't be empty" . "</span>" . "</br>";
            }

            if (!empty($this->phone) && !preg_match(("/([1-9])/"), $this->phone))
            {
                $this->error['phone2'] = "<span style='color:red'>" . "Invalid phone number" . "</span>" . "</br>";
            }

            if (empty($this->title)) {
                $this->error['title1'] = "<span style='color:red'>" . "Title is required" . "</span>" . "</br>";
            }


            if(empty($this->bio)){
                $this->error['BIO']="<span style='color:red'>" . "BIO is required" . "</span>" . "</br>";
            }
        }
        if (count($this->error) >0) {
            session_start();
            $_SESSION['fail'] = $this->error;

            header('location:Create.php');
        } else {
            $_POST["phone"] = filter_var($this->phone, FILTER_SANITIZE_STRING);
            $_POST["bio"] = filter_var($this->bio, FILTER_SANITIZE_STRING);
            $_POST["title"] = filter_var($this->title, FILTER_SANITIZE_STRING);

            $obj= new Abouts();

            $user_id= $_SESSION['user_info']['id'];

            $obj->setData($_POST,$user_id)->Store();

        }
    }

    public function updateValidation()
    {

            $this->error = array();
            if (isset($_POST)) {
                if (empty($this->phone)) {
                    $this->error['phone1'] = "<span style='color:red'>" . "Phone  can't be empty" . "</span>" . "</br>";
                }

                if (!empty($this->phone) && !preg_match(("/([1-9])/"), $this->phone))
                {
                    $this->error['phone2'] = "<span style='color:red'>" . "Invalid phone number" . "</span>" . "</br>";
                }

                if (empty($this->title)) {
                    $this->error['title1'] = "<span style='color:red'>" . "Title is required" . "</span>" . "</br>";
                }

                if (empty($this->bio)) {
                   $this->error['BIO'] ="<span style='color:red'>" . "BIO is Required" . "</span>" . "</br>";

                }

            }

            if (count($this->error) > 0) {

                session_start();
                $_SESSION['fail'] = $this->error;
               header('location:EditForm.php');
            } else {


                $obj= new Abouts();

                $user_id= $_SESSION['user_info']['id'];

                $obj->setData($_POST,$user_id)->update();

            }
        }



}