<?php



namespace App\Resumer\Abouts;

use App\Resumer\Abouts\Abouts;

class validation
{
    public $fname = '';
    public $sname = '';
    public $id = '';
    public $email = '';
    public $phone = '';
    public $program = '';
    public  $bio ='';
    public $title='';
    public $error = '';


    public function setData($data = '')
    {
        if (array_key_exists('fname', $data)) {
            $this->fname = $data['fname'];
        }
        if (array_key_exists('sname', $data)) {
            $this->sname = $data['sname'];
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('phone', $data)) {
            $this->phone = $data['phone'];
        }
        if (array_key_exists('title', $data)) {
            $this->title = $data['title'];
        }
        if (array_key_exists('bio', $data)) {
            $this->bio = $data['bio'];
        }
        if (array_key_exists('droopDown', $data)) {
            $this->program = $data['droopDown'];
        }
        return $this;
    }

    public function validForInsert()
    {
        $this->error = array();
        if (isset($_POST)) {
            if (empty($this->phone)) {
                $this->error['phone1'] = "<span style='color:red'>" . "Phone  can't be empty" . "</span>" . "</br>";
            }

            if (!empty($this->phone) && !preg_match(("/([1-9])/"), $this->phone))
            {
                $this->error['phone2'] = "<span style='color:red'>" . "Invalid phone number" . "</span>" . "</br>";
            }

            if (empty($this->title)) {
                $this->error['title1'] = "<span style='color:red'>" . "Title is required" . "</span>" . "</br>";
            }

            if (empty($this->bio)) {
                $this->error['bio1'] = "<span style='color:red'>" . "BIO is Required" . "</span>" . "</br>";
            }
        }
        if (count($this->error) > 0) {
            session_start();
            $_SESSION['fail'] = $this->error;

            header('location:create.php');
        } else {
            $_POST["phone"] = filter_var($this->phone, FILTER_SANITIZE_STRING);
            $_POST["bio"] = filter_var($this->bio, FILTER_SANITIZE_STRING);
            $_POST["title"] = filter_var($this->title, FILTER_SANITIZE_STRING);

            $obj= new Abouts();

            $user_id= $_SESSION['user_info']['id'];

            $obj->setData($_POST,$user_id)->Store();

        }
    }

    public function updateValidation()
    {
        $this->error = array();
        if (isset($_POST)) {
            if (empty($this->fname)) {
                $this->error['fname1'] = "<span style='color:red'>" . "First name Can't be empty" . "</span>" . "</br>";
            }
            if (!empty($this->fname) && !preg_match(("/([a-z,A-Z ])/"), $this->fname)) {
                $this->error['fname2'] = "<span style='color:red'>" . "Invalid character" . "</span>" . "</br>";
            }
            if (empty($this->sname)) {
                $this->error['sname1'] = "<span style='color:red'>" . "Second name Can't be empty" . "</span>" . "</br>";
            }
            if (!empty($this->sname) && !preg_match(("/([a-z,A-Z,])/"), $this->sname)) {
                $this->error['sname2'] = "<span style='color:red'>" . "invalid character" . "</span>" . "</br>";
            }
            if (empty($this->email)) {
                $this->error['email1'] = "<span style='color:red'>" . "Email  can't be empty" . "</span>" . "</br>";
            }
            if (!empty($this->email) && !filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
                $this->error['email2'] = "<span style='color:red'>" . "Enter Valid Email" . "</span>" . "</br>";
            }
            if (empty($this->phone)) {
                $this->error['phone1'] = "<span style='color:red'>" . "Phone  can't be empty" . "</span>" . "</br>";
            }
            if (!empty($this->phone) && !preg_match(("/([1-9])/"), $this->phone)) {
                $this->error['phone2'] = "<span style='color:red'>" . "Invalid phone number" . "</span>" . "</br>";
            }
            if (($_POST["droopDown"]) == "0") {
                $this->error['droopDown1'] = "<span style='color:red'>" . "Program  can't be empty" . "</span>" . "</br>";
            }
        }
        if (count($this->error) > 0) {
            session_start();
            $_SESSION['message'] = $this->error;

            header('location:edit.php');
        } else {
            $this->fname = filter_var($this->fname, FILTER_SANITIZE_STRING);
            $this->sname = filter_var($this->sname, FILTER_SANITIZE_STRING);
            $_POST["email"] = filter_var($this->email, FILTER_SANITIZE_STRING);
            $_POST["phone"] = filter_var($this->phone, FILTER_SANITIZE_STRING);
            $_POST["droopDown"] = filter_var($_POST["droopDown"], FILTER_SANITIZE_STRING);

            $obj = new Students();
             $obj->setData($_POST)->update();

        }
    }

}