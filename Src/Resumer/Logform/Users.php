<?php
namespace App\Resumer\Logform;
use pdo;
class Users
{
    public $id='';
    public $username='';
    public $email='';
    public $password = '';
    public $pdo='';

    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=cvbanks', 'root', '');
    }
    public function setData($data = '')
    {
        if (array_key_exists('username', $data)) {
            $this->username = $data['username'];
        }
        if (array_key_exists('password', $data)) {
            $this->password = $data['password'];
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        return $this;
    }
    public function Store()
    {


        try {
            $query  = "INSERT INTO `users` (`id`, `unique_id`, `username`, `email`, `password`, `token`,`user_role`) VALUES (:id, :uniuqeid ,:username,:email, :password,:token,:userroll )";

            $stmt =$this->pdo->prepare($query);
            $data = $stmt->execute(array(
                ':id' =>null,
                ':uniuqeid'=>uniqid(),
                ':username' => $this->username,
                ':email' => $this->email,
                ':password' => $this->password,
                ':token' => sha1($this->username),
                ':userroll' => 1
            ));
            if ($data) {
              header("location:create.php");
            }
        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }

    public function userAvialibilty(){
        $query="SELECT * FROM `users` WHERE `users`.`username` ='$this->username' or `users`.`email`='$this->email'";

        $stmt =$this->pdo->prepare($query);

       $stmt->execute();

        $data = $stmt->fetchAll();

        return $data;

    }

    public function login()
    {
        try {
            $query = "SELECT  * FROM `users` WHERE ( username = '$this->username' OR email = '$this->username') AND password = '$this->password'";

            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            if (!empty($data)) {
                $_SESSION['user_info'] = $data;
                header('location:../admin/index.php');
            } else {
                $_SESSION['fail'] = "Invalid Username, Email Or Password";
                header('location:index.php');
            }
            return $data;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }
}
