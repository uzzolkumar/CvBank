<?php
namespace App\Resumer\Educations;

use pdo;


class Educations
{
    public $id = '';
    public $userid='';
    public $title='';
    public $result = '';
    public $institute = '';
    public $majorsub = '';
    public $year = '';
    public $error='';
    public $Board='';
    public $Duration='';
    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=cvbanks', 'root', '');
    }
    public function setData($data = '',$userid='')
    {


        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('title', $data)) {
            $this->title = $data['title'];
        }
        if (array_key_exists('result', $data)) {
            $this->result = $data['result'];
        }
        if (array_key_exists('ins', $data)) {
            $this->institute = $data['ins'];
        }
        if (array_key_exists('msub', $data)) {
            $this->majorsub = $data['msub'];
        }
        if (array_key_exists('year', $data)) {
            $this->year = $data['year'];
        }
        if ($userid) {
            $this->userid = $userid;
        }
        if (array_key_exists('Board', $data)) {
            $this->Board = $data['Board'];
        }
        if (array_key_exists('Duration', $data)) {
            $this->Duration = $data['Duration'];
        }
        return $this;
    }
    public function Store()
    {
        try {
            $query  ="INSERT INTO `educations` (`id`, `user_id`, `title`, `institute`, `result`, `passing_year`, `main_subject`, `education_board`, `course_duration`) VALUES (:id, :user_id, :title, :instituteion, :result, :passingyear, :mainsub, :board, :duration)";


            $stmt =$this->pdo->prepare($query);

            $data = $stmt->execute(array(
              ':id'=>null,
                ':user_id'=>$this->userid,
                ':title'=>$this->title,
                ':instituteion'=>$this->institute,
                ':result'=>$this->result,
                ':passingyear'=>$this->year,
                ':mainsub'=>$this->majorsub,
                ':board'=>$this->Board,
                ':duration'=>$this->Duration
            ));

            if ($data) {
                $_SESSION['message']='Successfully ! Submitted';
                header("location:Create.php");
            }
        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }

    public function index()
    {

        try {

            $query  = "SELECT *FROM settings WHERE  user_id=$this->userid";

            $stmt =$this->pdo->prepare($query);
             $stmt->execute();

          $data =$stmt->fetch();

        return  $_SESSION['data']=$data;
        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }
    public function update()
    {


        try {


            $query  = "UPDATE `settings` SET  `title` =:title, `fullname` =:fullname, `description` =:description, `address` =:address, `featured_img` =:img WHERE `settings`.`user_id` = $this->userid";

            $stmt = $this->pdo->prepare($query);
            $data =   $stmt->execute(array(

                ':title'=>$this->title,
                ':fullname'=>$this->fullName,
                ':description'=>$this->description,
                ':address'=>$this->address,
                ':img'=>$this->img
            ));

            if ($data) {
                $_SESSION['message']='Successfully ! Updated';
                header("location:EditForm.php");
            }

        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }
    public function view()
    {


        try {

            $query  = "SELECT p.* ,c.* FROM educations as p , users as c WHERE p.user_id=c.id";

            $stmt =$this->pdo->prepare($query);
            $stmt->execute();

            $data =$stmt->fetchAll();

            return  $_SESSION['data']=$data;

        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }

    public function Delete()
    {


        try {
            $query  = "Delete From Settings WHERE  user_id=$this->userid";


            $stmt =$this->pdo->prepare($query);
            $result=  $stmt->execute();

            if($result)
            {
                $_SESSION['message'] ='Successfully Deleted';
                header("location:index.php");
            }

        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }
    public function PassingYear()
    {


        try {
            $query  = "Select *From year";


            $stmt =$this->pdo->prepare($query);
             $stmt->execute();
           return $result=$stmt->fetchAll();

        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }
}
