<?php
namespace App\Resumer\Settings;

use App\Resumer\Settings\Settings;


class validation
{
    public $id = '';
    public  $description ='';
    public $title='';
    public $error = '';
    public $img='';
    public $tmp='';
    public $allowedExtention='';
    public  $extention='';
    public $check='';
    public $address='';
    public $fullName='';

    public function setData($data = '')
    {

        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('fname', $data)) {
            $this->fullName = $data['fname'];
        }
        if (array_key_exists('address', $data)) {
            $this->address = $data['address'];
        }

        if (array_key_exists('title', $data)) {
            $this->title = $data['title'];
        }
        if (array_key_exists('description', $data)) {
         $this->description = $data['description'];
        }

        if($_FILES)
        {
               $this->img= $_FILES['img']['name'];
            $this->tmp_nam=$_FILES['img']['tmp_name'];
                $this->allowedExtention=array("jpg","jpeg","gif","png");
             $this->extention=end(explode(".",$this->img));

             $this->check=($this->extention=="jpg" || $this->extention=="png" || $this->extention=="gif" || $this->extention=="jpeg" );

        }
        return $this;
    }
    public function validForInsert()
    {
//        echo "<pre>";
//        print_r($this);
//        die();
        $this->error = array();
        if (isset($_POST)) {
            if(empty($_POST['img']) && $this->check == false)
            {
                $this->error['img1']='Please Select jpg/jpeg/png/gif image';
            }

            if (empty($this->title)) {
                $this->error['title1'] = "<span style='color:red'>" . "Title is required" . "</span>" . "</br>";
            }
            if (empty($this->fullName)) {
                $this->error['fname1'] = "<span style='color:red'>" . "Name is required" . "</span>" . "</br>";
            }
            if (empty($this->address)) {
                $this->error['add1'] = "<span style='color:red'>" . "Address is required" . "</span>" . "</br>";
            }
            if(empty($this->description)){
                $this->error['des']="<span style='color:red'>" . "Description  is required" . "</span>" . "</br>";
            }
        }
        if (count($this->error) >0) {
            session_start();
            $_SESSION['fail'] = $this->error;

            header('location:Create.php');
        } else {
            $obj= new Settings();

            $user_id= $_SESSION['user_info']['id'];

            $obj->setData($_POST,$user_id)->Store();

        }
    }

    public function updateValidation()
    {

        $this->error = array();
        if (isset($_POST)) {
            if(empty($_POST['img']) && $this->check == false)
            {
                $this->error['img1']='Please Select jpg/jpeg/png/gif image';
            }

            if (empty($this->title)) {
                $this->error['title1'] = "<span style='color:red'>" . "Title is required" . "</span>" . "</br>";
            }
            if (empty($this->fullName)) {
                $this->error['fname1'] = "<span style='color:red'>" . "Name is required" . "</span>" . "</br>";
            }
            if (empty($this->address)) {
                $this->error['add1'] = "<span style='color:red'>" . "Address is required" . "</span>" . "</br>";
            }
            if(empty($this->description)){
                $this->error['des']="<span style='color:red'>" . "Description  is required" . "</span>" . "</br>";
            }
        }
        if (count($this->error) >0) {
            session_start();
            $_SESSION['fail'] = $this->error;

            header('location:EditForm.php');
        } else {
            $obj= new Settings();

            $user_id= $_SESSION['user_info']['id'];

            $obj->setData($_POST,$user_id)->update();

        }
    }



}